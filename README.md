# A Bootable Alpine Container

Unlike many of the other images we offer, Alpine uses OpenRC rather than
systemd. That makes it small. But you're going to have to do a lot of work
yourself. This image basically boots to a non-interactive system and that's it.
You get to supply everything else.
