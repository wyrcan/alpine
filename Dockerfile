ARG TAG=latest
FROM alpine:${TAG}

# Install mkinitfs and disable initramfs generation
RUN apk add --update --no-cache mkinitfs linux-firmware-none
RUN echo "disable_trigger=1" >> /etc/mkinitfs/mkinitfs.conf

# Install and configure the kernel
RUN apk add --update --no-cache linux-lts
RUN cd /boot; ln -sf vmlinuz* wyrcan.kernel

# Install and configure openrc and eudev
RUN apk add --update --no-cache openrc eudev
RUN sed -i 's|^tty|#tty|' /etc/inittab
RUN ln -s /sbin/init /init
